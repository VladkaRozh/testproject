<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypt_name', function (Blueprint $table) {
            $table->increments('id');
            $table->text('symbol');
        });
        Schema::create('crypt_course', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('crypt_id');
            $table->float ('course',255,30)->nullable();
            $table->string('user_id');
            $table->dateTime('date');
        });
        Schema::create('users', function (Blueprint $table) {
            $table->string('id')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypt_name');
        Schema::dropIfExists('crypt_course');
        chema::dropIfExists('user');
    }
}
