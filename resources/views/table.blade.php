<body>
<?php $i=1; ?>


    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Symbol</th>
            <th>Course</th>
            <th>UserId</th>
        </tr>
        </thead>
        <tbody>
        @foreach($show as $s)
        <tr>
            <th scope="row">{{$i}}</th>
            <td>{{$s->symbol}}</td>
            <td>{{$s->course}}</td>
            <td>{{$s->user_id}}</td>
        </tr>
            <?php $i++?>
            @endforeach
        </tbody>
    </table>
</body>