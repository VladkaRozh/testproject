
Laravel v.5.5, php 7.0, MySQL 5.7.21, Apache 2.4.33, OS Windows

Установка проекта:
composer install 
php artisane migrate

Описание проекта:

Консольная команда php artisan add:crypt_info
Добавляет курсы криптовалют в базу данных.

localhost/add - страница с формой для добавления курса в ручную c минимальной валидацией данных
localhost/show - страница с выводом последних курсов

Структура БД:

Таблица crypt_course - таблица курсов 
	crypt_id - id криптовалюты
	course - курс криптовалюты
	user_id - id юзера, который добавил валюту в ручную
	data - дата добавления
таблица crypt_name - таблица имен валюты
	symbol - символ валюты
таблица users - таблица с рандомными id пользователей добавлявших курсы
	id - рандомный айди пользователя 

Скриншоты веб-страниц в папке screenshot

