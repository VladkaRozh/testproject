<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AddCourseModel extends Model
{
    static function checkSymbol($data)
    {
        return DB::table('crypt_name')->where('symbol', '=', $data['crypt_name'])->first(['id']);
    }

    static function insertToSymbol($data)
    {
        return DB::table('crypt_name')->insertGetId(['symbol' => $data['crypt_name']]);
    }

    static function insertToCourse($data)
    {
        DB::table('crypt_course')->insert([
            'course' => $data['course'],
            'crypt_id' => $data['crypt_id'],
            'date' => $data['date'],
            'user_id' => $data['user_id']]);
    }
}
