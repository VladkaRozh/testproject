<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class AddUserModel extends Model
{
    static function insertUsers($data)
    {
        return DB::table('users')->insertGetId(['id' => $data['user_id']]);
    }
}
