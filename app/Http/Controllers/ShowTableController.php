<?php

namespace App\Http\Controllers;

use App;
use DB;

class ShowTableController extends Controller
{
    public function show()
    {
        $show = App\ShowTableModel::show();
        return view('table', compact('show'));
    }
}
