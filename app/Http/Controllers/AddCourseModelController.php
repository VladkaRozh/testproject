<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class AddCourseModelController extends Controller
{
    public function add(Request $request)
    {
        $validatedata       = $request->validate([
            'crypt_name' => 'required',
            'course'     => 'required|integer'
        ]);
        $data['crypt_name'] = $request['crypt_name'];
        $data['course']     = $request['course'];
        $data['date']       = date("Y-m-d H:i:s");
        $data['user_id']    = str_random(10);
        $check              = App\AddCourseModel::checkSymbol($data);
        if ($check == null) {
            $data['crypt_id'] = App\AddCourseModel::insertToSymbol($data);
            App\AddUserModel::insertUsers($data);
            App\AddCourseModel::insertToCourse($data);
            return view('form');
        } else {
            $id               = $check->id;
            $data['crypt_id'] = $id;
            App\AddUserModel::insertUsers($data);
            App\AddCourseModel::insertToCourse($data);
            return view('form');
        }
    }
    public function show(){
        return  view('form');
    }
}
