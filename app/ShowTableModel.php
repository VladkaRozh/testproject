<?php

namespace App;


use DB;
use Illuminate\Database\Eloquent\Model;

class ShowTableModel extends Model
{
    static function show()
    {
        $bit_ids = DB::table('crypt_course')->select(DB::raw('max(id) as id'))
            ->groupBy('crypt_id')
            ->get()->pluck('id')->toArray();
        return DB::table('crypt_name')->leftjoin('crypt_course', 'crypt_name.id', '=', 'crypt_course.crypt_id')
            ->leftjoin('users', 'crypt_name.id', '=', 'users.id')
            ->whereIn('crypt_course.id', $bit_ids)
            ->get();
    }
}