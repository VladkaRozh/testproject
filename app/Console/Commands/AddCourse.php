<?php

namespace App\Console\Commands;

use App;
use Illuminate\Console\Command;

class addCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:crypt_info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add tickers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url  = 'https://api.hitbtc.com/api/2/public/ticker';
        $json = file_get_contents($url);
        $j    = json_decode($json, true);
        foreach ($j as $v) {
            $data['crypt_name'] = $v['symbol'];
            $data['course']     = $v['ask'];
            $v['timestamp']     = date("Y-m-d H:i:s");
            $data['date']       = $v['timestamp'];
            $data['user_id']    = 0;
            $check              = App\AddCourseModel::checkSymbol($data);
            if ($check == null) {
                $data['crypt_id'] = App\AddCourseModel::insertToSymbol($data);
                App\AddCourseModel::insertToCourse($data);
            } else {
                $id               = $check->id;
                $data['crypt_id'] = $id;
                App\AddCourseModel::insertToCourse($data);
            }
        }
    }
}
